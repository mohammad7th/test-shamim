module.exports = {
    content: [
        "./index.html",
        "./src/**/*.{vue,js,ts,jsx,tsx}",
        "./safelist.txt",
    ],
    theme: {
        extend: {
            colors: {
                primary1: "var(--primary-1)",
                primary2: "var(--primary-2)",
                primary: "var(--primary)",
                primary4: "var(--primary-4)",
                primary5: "var(--primary-5)",
                /* Secondary Colors */
                secondary1: "var(--secondary-1)",
                secondary2: "var(--secondary-2)",
                secondary: "var(--secondary)",
                secondary4: "var(--secondary-4)",
                secondary5: "var(--secondary-5)",
                /* Background Color */
                background: "var(--bg-color)",
                background2: "var(--bg-color-2)",
                /* Neutral Colors */
                neutral1: "var(--neutral-1)",
                neutral2: "var(--neutral-2)",
                neutral3: "var(--neutral-3)",
                neutral4: "var(--neutral-4)",
                neutral5: "var(--neutral-5)",
                neutral6: "var(--neutral-6)",
                neutral7: "var(--neutral-7)",
                neutral8: "var(--neutral-8)",
                neutral9: "var(--neutral-9)",
                neutral10: "var(--neutral-10)",
                /* Text Colors */
                error: "var(--error)",
                success: "var(--success)",
                disabled: "var(--disabled)",
                danger: "var(--danger)",
            },

        },
    },
    plugins: [],
}