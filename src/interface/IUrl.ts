import type { Method } from "../enums";
interface IUrl {
	path: string;
	method: Method;
	param?: string;
	query?: string;
}

export default IUrl;