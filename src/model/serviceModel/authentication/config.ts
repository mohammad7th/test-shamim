export default class Config {
    authorization: string;
    apiEndpoint: string;
}