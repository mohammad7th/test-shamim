import { useRouter } from "vue-router";
import { configStore } from "@/store";

export function redirectIfNotLogin() {
    if (!isLoggedIn()) {
        configStore.commit("remove");
        useRouter().push("/login");
        return;
    }
}
export function isLoggedIn() {
    if (!configStore?.state?.configuration?.authorization) {
        return false;
    }
    return true;
}