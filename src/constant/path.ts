const Path = {
    home: "/",
    login: "/login",
    post_list: "/posts",
    post: "/posts/:id",
    photo_list: "/photos",
    photo: "/photos/:id",
    logout: "/logout",
};
export default Path;