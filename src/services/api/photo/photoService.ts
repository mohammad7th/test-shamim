import type { ServiceModel } from "@/model";
import { Server, ResponseModel } from "@/server";
import Url from "../url";

class PhotoService {
    url = new Url();
    constructor() {

    }
    async list(): Promise<ResponseModel<ServiceModel.photo.Photo[]>> {
        try {
            const response = await Server<ServiceModel.photo.Photo[]>(this.url.Photo.list);
            if (response?.status) {
                return new ResponseModel<ServiceModel.photo.Photo[]>(true, response.data);
            }
            return new ResponseModel<ServiceModel.photo.Photo[]>(false, null!, response.message);
        } catch (exp) {
            return new ResponseModel<ServiceModel.photo.Photo[]>(false, null!, exp.message);
        }
    }
    async getPhoto(model: number): Promise<ResponseModel<ServiceModel.photo.Photo>> {
        try {
            const response = await Server<ServiceModel.photo.Photo>(this.url.Photo.getPhoto, model);
            if (response?.status) {
                return new ResponseModel<ServiceModel.photo.Photo>(true, response.data);
            }
            return new ResponseModel<ServiceModel.photo.Photo>(false, null!, response.message);
        } catch (exp) {
            return new ResponseModel<ServiceModel.photo.Photo>(false, null!, exp.message);
        }
    }
}
export default PhotoService;
