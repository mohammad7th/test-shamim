import type { ServiceModel } from "@/model";
import { Server, ResponseModel } from "@/server";
import Url from "../url";

class AuthenticationService {
    url = new Url();
    constructor() {

    }
    async login(model: ServiceModel.authentication.LoginRequest): Promise<ResponseModel<ServiceModel.authentication.LoginResponse>> {
        try {
            //#region validation
            if (!model) {
                return new ResponseModel<ServiceModel.authentication.LoginResponse>(false, null!, "An error has occurred");
            }
            //#endregion
            const response = await Server<ServiceModel.authentication.LoginResponse>(this.url.Auth.login, model);
            console.log('response',response);
            
            if (response?.status) {
                return new ResponseModel<ServiceModel.authentication.LoginResponse>(true, response.data);
            }
            return new ResponseModel<ServiceModel.authentication.LoginResponse>(false, null!, response.message);
        } catch (exp) {
            return new ResponseModel<ServiceModel.authentication.LoginResponse>(false, null!, exp.message);
        }
    }
}
export default AuthenticationService;
