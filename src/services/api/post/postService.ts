import type { ServiceModel } from "@/model";
import { Server, ResponseModel } from "@/server";
import Url from "../url";

class PostService {
    url = new Url();
    constructor() {

    }
    async list(): Promise<ResponseModel<ServiceModel.post.Post[]>> {
        try {
            const response = await Server<ServiceModel.post.Post[]>(this.url.Post.list);
            if (response?.status) {
                return new ResponseModel<ServiceModel.post.Post[]>(true, response.data);
            }
            return new ResponseModel<ServiceModel.post.Post[]>(false, null!, response.message);
        } catch (exp) {
            return new ResponseModel<ServiceModel.post.Post[]>(false, null!, exp.message);
        }
    }
    async getPost(model: number): Promise<ResponseModel<ServiceModel.post.Post>> {
        try {
            const response = await Server<ServiceModel.post.Post>(this.url.Post.getPost, model);
            if (response?.status) {
                return new ResponseModel<ServiceModel.post.Post>(true, response.data);
            }
            return new ResponseModel<ServiceModel.post.Post>(false, null!, response.message);
        } catch (exp) {
            return new ResponseModel<ServiceModel.post.Post>(false, null!, exp.message);
        }
    }
}
export default PostService;
