import { Method } from "@/enums";
import type { IUrl } from "@/interface";

class Url {
    public Auth: Auth = new Auth();
    public Post: Post = new Post();
    public Photo: Photo = new Photo();
}

class Auth {
    public login: IUrl = {
        path: "https://reqres.in/api/login",
        method: Method.POST,
    };
}
class Post {
    public list: IUrl = {
        path: "https://jsonplaceholder.typicode.com/posts",
        method: Method.GET,
    };
    public getPost: IUrl = {
        path: "https://jsonplaceholder.typicode.com/posts/",
        method: Method.GET,
        param: 'id'
    };
}
class Photo {
    public list: IUrl = {
        path: "https://jsonplaceholder.typicode.com/photos",
        method: Method.GET,
    };
    public getPhoto: IUrl = {
        path: "https://jsonplaceholder.typicode.com/photos/",
        method: Method.GET,
        param: 'id'
    };
}

export default Url;