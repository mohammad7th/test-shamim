enum TextColor {
    /* Primary Colors */
    PRIMARY1 = "text-primary1",
    PRIMARY2 = "text-primary2",
    PRIMARY = "text-primary",
    PRIMARY4 = "text-primary4",
    PRIMARY5 = "text-primary5",
    /* Secondary Colors */
    SECONDARY1 = "text-secondary1",
    SECONDARY2 = "text-secondary2",
    SECONDARY = "text-secondary",
    SECONDARY4 = "text-secondary4",
    SECONDARY5 = "text-secondary5",
    /* Background Color */
    BGCOLOR = "text-bg-color",
    /* Neutral Colors */
    NEUTRAL1 = "text-neutral1",
    NEUTRAL2 = "text-neutral2",
    NEUTRAL3 = "text-neutral3",
    NEUTRAL4 = "text-neutral4",
    NEUTRAL5 = "text-neutral5",
    NEUTRAL6 = "text-neutral6",
    NEUTRAL7 = "text-neutral7",
    NEUTRAL8 = "text-neutral8",
    /* Text Colors */
    ERROR = "text-error",
    SUCCESS = "text-success",
    DISABLED = "text-disabled",
    DANGER = "text-danger",
}
enum BgColor {
    /* Primary Colors */
    PRIMARY1 = "bg-primary1",
    PRIMARY2 = "bg-primary2",
    PRIMARY = "bg-primary",
    PRIMARY4 = "bg-primary4",
    PRIMARY5 = "bg-primary5",
    /* Secondary Colors */
    SECONDARY1 = "bg-secondary1",
    SECONDARY2 = "bg-secondary2",
    SECONDARY = "bg-secondary",
    SECONDARY4 = "bg-secondary4",
    Secondary5 = "bg-secondary5",
    /* Background Color */
    BGCOLOR = "bg-bg-color",
    /* Neutral Colors */
    NEUTRAL1 = "bg-neutral1",
    NEUTRAL2 = "bg-neutral2",
    NEUTRAL3 = "bg-neutral3",
    NEUTRAL4 = "bg-neutral4",
    NEUTRAL5 = "bg-neutral5",
    NEUTRAL6 = "bg-neutral6",
    NEUTRAL7 = "bg-neutral7",
    NEUTRAL8 = "bg-neutral8",
    /* text Colors */
    ERROR = "bg-error",
    SUCCESS = "bg-success",
    DISABLED = "bg-disabled",
    DANGER = "bg-danger",
}
enum Color {
    PRIMARY1 = "--primary-1",
    PRIMARY2 = "--primary-2",
    PRIMARY = "--primary",
    PRIMARY4 = "--primary-4",
    PRIMARY5 = "--primary-5",
    /* Secondary Colors */
    SECONDARY1 = "--secondary-1",
    SECONDARY2 = "--secondary-2",
    SECONDARY = "--secondary",
    SECONDARY4 = "--secondary-4",
    SECONDARY5 = "--secondary-5",
    /* Background Color */
    BGCOLOR = "--bg-color",
    /* Neutral Colors */
    NEUTRAL1 = "--neutral-1",
    NEUTRAL2 = "--neutral-2",
    NEUTRAL3 = "--neutral-3",
    NEUTRAL4 = "--neutral-4",
    NEUTRAL5 = "--neutral-5",
    NEUTRAL6 = "--neutral-6",
    NEUTRAL7 = "--neutral-7",
    NEUTRAL8 = "--neutral-8",
    /* Text Colors */
    ERROR = "--error",
    SUCCESS = "--success",
    DISABLED = "--disabled",
    DANGER = "--danger",
}
export { Color, TextColor, BgColor };
