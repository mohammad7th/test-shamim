export enum SpinnerSize {
  XS = 1,
  SM = 1.2,
  MD = 1.4,
  LG = 1.6,
  XL = 1.8,
  DEFAULT = 1,
}
