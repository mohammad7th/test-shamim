export enum CalendarTypeEnums {
	RANGE = "range",
	SINGLE = "single",
	MULTIPLE = "multiple",
	SHOW_DATES = "showdates",
}
