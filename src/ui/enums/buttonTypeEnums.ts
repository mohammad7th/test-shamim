export enum ButtonTypeEnums {
	DEFAULT = "contained",
	CONTAINED = "contained",
	OUTLINED = "outlined",
	TEXT = "text",
}
