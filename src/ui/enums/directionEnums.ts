export enum DirectionEnums {
	DEFAULT = "ltr",
	RTL = "rtl",
	LTR = "ltr",
}
