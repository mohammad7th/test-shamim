export enum RoundedEnums {
	NONE = "rounded-none",
	NORMAL = "rounded",
	FULL = "rounded-full",
}
