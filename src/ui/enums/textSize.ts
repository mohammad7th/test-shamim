export enum TextSize {
    XS = "text-xs ",
    SM = "text-sm ",
    BASE = "text-base",
    LG = "text-lg ",
    SIZEXL = "text-xl ",
    SIZE2XL = "text-2xl ",
    SIZE3XL = "text-3xl ",
    SIZE4XL = "text-4xl ",
    SIZE5XL = "text-5xl ",
    SIZE6XL = "text-6xl ",
    SIZE7XL = "text-7xl ",
    SIZE8XL = "text-8xl ",
    SIZE9XL = "text-9xl ",
}