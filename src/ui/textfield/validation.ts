const englishRegex = /^[a-zA-Z0-9\_\-\.\@\n?]+$/;
const farsiRegex = /^([\u0600-\u06FF\s])|(^[0-9]*$)+$/;
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const urlRegex = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;

export function isValidEmail(email: string) {
  if (!emailRegex.test(email)) {
    return false;
  }
  return true;
}

export function isValidUrl(url: string) {
  if (!urlRegex.test(url)) {
    return false;
  }
  return true;
}

export function isValidTel(tel: string) {
  const telRegex = /^([0-9]+)$/;

  if (!telRegex.test(tel)) {
    return false;
  }
  return true;
}

export function isPersian(val: string) {
  if (farsiRegex.test(val)) {
    return true;
  }
  return false;
}

export function isEnglish(val: string) {
  if (englishRegex.test(val)) {
    return true;
  }
  return false;
}

export function isValidPassword(val: string) {
  if (val && val.length < 6) {
    return false;
  }
  return true;
}
