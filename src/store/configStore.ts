import { createStore } from 'vuex';
import { ServiceModel } from '@/model';
const configStore = createStore({
	state() {
		return {
			configuration: JSON.parse(localStorage.getItem("__configuration") || '{}')
		}
	},
	getters: {
		get(state) {
			return state.configuration;
		}
	},
	mutations: {
		set(state: any, data: ServiceModel.authentication.Config) {
			state.configuration = data;
			localStorage.setItem("__configuration", JSON.stringify(data));
		},
		remove(state) {
			state.configuration = null;
			localStorage.removeItem("__configuration");
		},
	},
})
export default configStore
