import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router';
import Route from "@/routes";
import '@/index.css';
import '@/assets/css/reset.scss';
import '@/assets/css/theme.scss';

const route = createRouter({
    history: createWebHistory(),
    routes: Route
});
route.beforeEach((to, from, next) => {
    document.title = `${to.meta.title}`;
    next();
});
const app = createApp(App);
app.use(route);
app.mount('#app');
