class ResponseModel<T> {
	constructor(_status: boolean, _data: T, _message?: string, _code?: number) {
		if (_status) {
			this.data = _data;
		}
		this.status = _status;
		this.message = _message!;
		this.code = _code!;
	}

	public data!: T;
	public status: boolean;
	public message: string;
	public code: number;
}

export default ResponseModel;
