import { Method } from "@/enums";
import type { IUrl } from "@/interface";
import ResponseModel from "@/server/responseModel";
import axios, { AxiosError } from "axios";

async function server<T>(
    url: IUrl,
    data?: any,
    isQueryString = false
): Promise<ResponseModel<T>> {
    try {
        const usersAxios = axios.create();
        if (url && url.path && url.method) {
            if (url.method === Method.GET) {
                if (url.param) {
                    url.path += data;
                }
                const res = await usersAxios.get(url.path);
                return new ResponseModel<T>(
                    true,
                    res.data,
                    "",
                );
            }
            if (url.method === Method.POST) {
                const res = await usersAxios.post(url.path, data);
                console.log('res', res);

                return new ResponseModel<T>(
                    true,
                    res.data,
                    "",
                );
            }

        }
        return new ResponseModel<T>(false, null!, "");
    } catch (exp) {
        if (axios.isAxiosError(exp)) {

            if ((exp as any).response?.data?.error) {
                return new ResponseModel<T>(
                    false,
                    null!,
                    (exp as any).response?.data.error,
                    exp.response?.status
                );
            }
            return new ResponseModel<T>(
                false,
                null!,
                exp.message,
                exp.response?.status
            );
        } else {
            console.log('exception:::', exp);
            return new ResponseModel<T>(
                false,
                null!,
                "check console"
            );
        }
    }
}
export default server;
