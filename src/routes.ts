import { Path } from "@/constant";
import Login from "@/components/authentication/login/Index.vue";
import Logout from "@/components/authentication/logout/Index.vue";
import Home from "@/components/home/Index.vue";
import PostList from "@/components/post/PostList.vue";
import Post from "@/components/post/Post.vue";
import PhotoList from "@/components/photo/PhotoList.vue";
import Photo from "@/components/photo/Photo.vue";

const Route = [
    { name: "Login", path: Path.login, component: Login, meta: { title: 'Login' }, },
    { name: "Logout", path: Path.logout, component: Logout, meta: { title: 'Logout' }, },
    { name: "Home", path: Path.home, component: Home, meta: { title: 'Home', layout: 'main' }, },
    { name: "Post", path: Path.post, component: Post, meta: { title: 'Post', layout: 'main' }, },
    { name: "Post List", path: Path.post_list, component: PostList, meta: { title: 'Post List', layout: 'main' }, },
    { name: "Photo", path: Path.photo, component: Photo, meta: { title: 'Photo', layout: 'main' }, },
    { name: "Photo List", path: Path.photo_list, component: PhotoList, meta: { title: 'Photo List', layout: 'main' }, },
];
export default Route;
